
FROM node:16-alpine


COPY package*.json ./
RUN npm i -P
COPY . .

RUN mkdir /dist
RUN cp -r ./lib /dist/
RUN cp -r ./node_modules /dist/
RUN cp package*.json /dist/

FROM gcsboss/nodecaf:0.12
COPY --from=0 /dist /app

USER root
