/* eslint-env mocha */
// var wtf = require('wtfnode');

const Docker = require('dockerode');
const assert = require('assert');
const redis = require('nodecaf-redis');
const muhb = require('muhb');
const http = require('http');

process.env.NODE_ENV = 'testing';

const init = require('../lib/main');

const DOCKER_URL = process.env.DOCKER_URL || 'localhost';
const MONGO_URL = process.env.MONGO_URL || 'mongodb://localhost:27017';
const MC_HOST = 'http://' + (process.env.MC_HOST || 'localhost') + ':1080';

const DEFAULT_CONF = {
    docker: { host: DOCKER_URL, port: 2375 },
    redis: {
        host: process.env.REDIS_HOST || 'localhost',
        type: 'redispub',
        port: 6379,
        channel: 'roorar'
    },
    smtp: {
        host: process.env.MC_HOST || 'localhost',
        port: 1025,
        to: 'jubi@ju.ju',
        pool: true
    },
    gitlab: {
        project: 23489,
        token: 'bar',
        url: 'http://localhost:12345',
        labels: [ 'level::{level}', 'barbar' ]
    },
    emit: { event: 'foobar' }
};

let app, docker;
let blab;

async function sleep(ms){
    await new Promise(done => setTimeout(done, ms));
}

before(async function(){
    this.timeout(20e3);
    docker = new Docker(DEFAULT_CONF.docker);
    await docker.pull('mhart/alpine-node:slim-14');
    await sleep(4000);

    // kill possible remaining containers from previous failed runs
    const containers = await docker.listContainers();
    const containersToKill = containers.filter((container) =>
        container?.Labels?.['com.docker.compose.project'] === 'foobar');
    for(const container of containersToKill) {
        const containerInstance = docker.getContainer(container.Id);
        await containerInstance.kill();
        console.log(`Container with ID ${container.Id} has been killed.`);
    }

    blab = await docker.createContainer({
        Image: 'mhart/alpine-node:slim-14',
        Tty: false,
        Init: true,
        Labels: {
            'com.docker.compose.project': 'foobar',
            'com.docker.compose.service': 'bazbaz'
        },
        Cmd: [ 'node', '-e', 'setInterval(() => console.log(\'28234768\'), 600)' ]
    });
    await blab.start();
    await sleep(1000);
});

beforeEach(function(){
    app = init();
    app.setup(DEFAULT_CONF);
});

after(async function(){
    await blab.kill();
    await docker.pruneContainers();
});

afterEach(async function(){
    await app.stop();
});

async function startWaitForIt(payload = '28234768'){
    const value = JSON.stringify(JSON.stringify(payload));
    const container = await docker.createContainer({
        Image: 'mhart/alpine-node:slim-14',
        Tty: false,
        Init: true,
        Labels: {
            'com.docker.compose.project': 'foobar',
            'com.docker.compose.service': 'bazbaz'
        },
        Cmd: [ 'node', '-e', 'setTimeout(() => console.log(' + value + '), 1000);' +
            'setTimeout(Function.prototype, 10000)']
    });
    await container.start();
    return container;
}

describe('Startup', function(){

    it('Should boot just fine', async function(){
        this.timeout(12000);
        await app.start();
        await app.stop();
    });

    it('Should fail when cannot connect to docker', async function(){
        this.timeout(2200);
        app.setup({ docker: { host: 'http://12.12.12.12' } });
        await assert.rejects(app.start());
    });

});

describe('Containers', function(){

    it('Should attach new containers', async function(){
        this.timeout(12000);
        await app.start();
        const c = await startWaitForIt();
        await sleep(2000);
        assert(c.id in app.global.containers);
        await c.kill();
    });

    it('Should attach containers that were running since before', async function(){
        this.timeout(5000);
        await app.start();
        await sleep(500);
        assert(blab.id in app.global.containers);
    });

    it('Should forget containers after they die', async function(){
        this.timeout(12000);
        await app.start();
        const c = await startWaitForIt();
        await sleep(800);
        await c.kill();
        await sleep(1400);
        assert(!(c.id in app.global.containers));
    });

    it('Should not attach containers filtered out in the global line', async function(){
        this.timeout(8000);
        app.setup({ pipeline: [ { stack: 'none' } ] });
        await app.start();
        const c = await startWaitForIt();
        await sleep(2000);
        assert(!app.global.containers[c.id].stream);
        await c.kill();
    });

});

describe('Pipeline', function(){

    it('Should fail on bad job type', async function(){

        app.setup({ pipeline: [
            { badtype: {} }
        ] });

        await assert.rejects(app.start());
    });

    it('Should only process logs of certain level and above', function(done){
        this.timeout(8000);

        process.once('foobab', function(msg){
            assert.strictEqual(msg.level, 'warn');
            done();
        });

        (async function(){
            app.setup({ pipeline: [
                { level: 'warn' },
                { emit: { event: 'foobab' } }
            ] });
            await app.start();

            const c = await startWaitForIt({ level: 'info', msg: 876 });
            await sleep(1000);
            const c2 = await startWaitForIt({ level: 'warn', msg: 879 });

            await sleep(1000);

            await c.kill();
            await c2.kill();
        })();
    });

    it('Should support level strings in a case INsenstive manner', function(done){
        this.timeout(8000);

        process.once('foobab', function(msg){
            assert.strictEqual(msg.level, 'warn');
            done();
        });

        (async function(){
            app.setup({ pipeline: [
                { level: 'warn' },
                { emit: { event: 'foobab' } }
            ] });
            await app.start();

            const c = await startWaitForIt({ level: 'INFO', msg: 876 });
            await sleep(1000);
            const c2 = await startWaitForIt({ level: 'wArN', msg: 879 });

            await sleep(1000);

            await c.kill();
            await c2.kill();
        })();
    });

});


describe('Transforms', function(){

    it('Should apply keys to log entry', function(done){

        process.once('foobar', function(msg){
            assert.strictEqual(msg.test, 'foo');
            done();
        });

        (async function(){
            app.setup({ pipeline: [
                { apply: { test: 'foo' } },
                { emit: { event: 'foobar' } }
            ] });
            await app.start();
        })();
    });

    it('Should ensure given key is a datetime', function(done){
        (async function(){
            app.setup({ pipeline: [
                { time: 'foo' },
                { emit: { event: 'foobar' } }
            ] });
            await app.start();

            process.once('foobar', function(msg){
                assert(msg.foo instanceof Date);
                done();
            });
        })();
    });

    it('Should add specified info to log entry', function(done){

        app.setup({ pipeline: [
            { includeSource: true },
            { emit: { event: 'foobak' } }
        ] });
        app.start().then(() => {
            process.once('foobak', function(msg){
                assert.strictEqual(msg.source.id, blab.id);
                done();
            });
        });
    });

    it('Should wrap log entry in another object', function(done){

        process.once('foobar', function(msg){
            assert(msg.final.message);
            done();
        });

        (async function(){
            app.setup({ pipeline: [
                { envelope: 'final' },
                { emit: { event: 'foobar' } }
            ] });
            await app.start();
        })();
    });

    it('Should not try and reparse a JSON object on branches', function(done){
        this.timeout(12000);

        process.once('unhandledRejection', done);
        process.once('uncaughtException', done);

        process.once('foobat', function(msg){
            assert(msg.message);
            process.off('unhandledRejection', done);
            process.off('uncaughtException', done);
            done();
        });

        (async function(){
            app.setup({
                pipeline: [
                    {
                        branch: [
                            { services: [ 'bazbaz' ] },
                            { replace: '2', with: '_' },
                            { time: 'time' },
                            { emit: { event: 'foobat' } }
                        ]
                    },
                    { includeSource: true },
                    { emit: { event: 'foobat' } }
                ]
            });
            await app.start();
        })();
    });

});

describe('Transports', function(){

    it('Should insert log entries through mongo transport', async function(){
        this.timeout(12000);
        app.setup({ pipeline: [
            { emit: { event: 'foobar' } },
            { mongo: { url: MONGO_URL } }
        ] });
        await app.start();
        await new Promise(done => process.once('foobar', done));
        await sleep(2000);

        const { MongoClient } = require('mongodb');
        const client = new MongoClient(MONGO_URL, { useUnifiedTopology: true });
        await client.connect();
        const db = client.db('Eavesdocker');

        try{
            const r = db.collection('Log_Entries').find({}, { sort: { _id: -1 } });
            const a = await r.toArray();
            assert.strictEqual(a[0].message, 28234768);
        }
        finally{
            await db.dropDatabase();
            client.close();
        }
    });

    it('Should publish log entries to redis channel', function(done){
        this.timeout(12000);
        (async function(){
            app.setup({ pipeline: [
                { time: 'time' },
                { stack: 'foobar' },
                { webhook: { url: 'http://localhost:8765/test' } },
                { redispub: { $inherit: 'redis' } }
            ] });
            await app.start();
            const client = await redis(DEFAULT_CONF.redis);
            client.subscribe('roorar', function(data){
                assert.strictEqual(JSON.parse(data).message, 28234768);
                client.close().then(() => done());
            });
        })();

    });

    it('Should send an e-mail', async function(){
        this.timeout(12000);

        const { status } = await muhb.delete(MC_HOST + '/messages');
        assert.strictEqual(status, 204);
        app.setup({ pipeline: [
            { stack: 'foobar' },
            { apply: { foonull: null, footag: '<h1>test</h1>' } },
            { email: { $inherit: 'smtp' } }
        ] });
        await app.start();
        const c = await startWaitForIt();
        await sleep(6000);
        await c.kill();
        await app.stop();

        const { body } = await muhb.get(MC_HOST + '/messages');
        const obj = JSON.parse(body);
        // console.log(obj);
        assert.strictEqual(obj[0].subject.indexOf('debug -'), 0);

        const { body: source } = await muhb.get(MC_HOST + '/messages/' + obj[0].id + '.html');
        assert(source.indexOf('<code>null</code>') > 0);
        assert(source.indexOf('&#60;h1&#62;') > 0);
    });

    it('Should run an http request', function(done){
        this.timeout(5000);
        const s = http.createServer((req, res) => {
            let gotBody = '';
            req.setEncoding('utf-8');
            req.on('data', buf => gotBody += buf);
            req.on('end', () => {
                res.end();
                s.close();
                JSON.parse(gotBody);
                assert.strictEqual(req.url, '/test');
                done();
            });
        }).listen(8765);

        app.setup({ pipeline: [
            { webhook: { url: 'http://localhost:8765/test' } }
        ] });
        app.start();
    });

    it('Should create a gitlab issue', function(done){

        const Nodecaf = require('nodecaf');
        const fakeGl = new Nodecaf({ autoParseBody: true, conf: { port: 12345 }, api: function({ post }){

            post('/api/v4/projects/:id/issues', async ({ params, headers, body, res }) => {
                assert.strictEqual(params.id, '23489');
                assert.strictEqual(headers['private-token'], 'bar');
                assert.strictEqual(params.id, '23489');
                assert.strictEqual(body.labels[0], 'level::debug');
                assert.strictEqual(body.labels[1], 'barbar');
                assert.strictEqual(body.title.indexOf('debug - '), 0);
                res.end();

                await fakeGl.stop();
                done();
            });

        } });

        fakeGl.start();
        app.setup({ pipeline: [ { gitlab: { $inherit: 'gitlab' } } ] });
        app.start();
    });

    it('Should not crash on GitLab error esponse', function(done){

        const Nodecaf = require('nodecaf');
        const fakeGl = new Nodecaf({ conf: { port: 12345 }, api: function({ post }){

            post('/api/v4/projects/:id/issues', async ({ res }) => {
                res.status(400).end();

                await fakeGl.stop();
                done();
            });

        } });

        fakeGl.start();
        app.setup({ pipeline: [ { gitlab: { $inherit: 'gitlab' } } ] });
        app.start();
    });

    it('Should push a gitlab alert', function(done){
        const FAKE_PORT = 12345;

        const Nodecaf = require('nodecaf');
        const fakeGl = new Nodecaf({
            autoParseBody: true,
            conf: { port: FAKE_PORT },
            api: function({ post }){

                post('/', async ({ headers, body, res }) => {
                    assert.strictEqual(headers.authorization, 'Basic OmFiYw==');
                    assert.strictEqual(body.title, 'testing - 28234768');
                    assert.strictEqual(body.fingerprint, 'da39a3ee5e6b4b0d3255bfef95601890afd80709');
                    res.end();

                    await fakeGl.stop();
                    done();
                });

            }
        });

        fakeGl.start();
        app.setup({ pipeline: [
            {
                gitlabAlert: {
                    url: 'http://localhost:' + FAKE_PORT,
                    key: 'abc',
                    service: 'static-service',
                    fingerprintFields: [ 'msg' ],
                    env: 'testing'
                }
            }
        ] });
        app.start();
    });

});

describe('Redis Health Reporter', function(){

    it('Should keep online containers info up to date in a redis instance', async function(){
        this.timeout(10000);

        const client = await redis(DEFAULT_CONF.redis);

        app.setup({
            containerHealthThreshold: 1,
            health: { $inherit: 'redis' },
            pipeline: [ { emit: { event: 'emit' } } ]
        });
        await app.start();

        const c = await startWaitForIt();

        await sleep(3000);

        const json = await client.get('eavesdocker:containers:' + c.id.substr(0, 12));

        await c.kill();
        await app.stop();

        await client.close();
        assert(json);
    });

    it('Should keep node info up to date in a redis instance', async function(){
        this.timeout(10000);

        const { ID, Swarm } = await docker.info();
        const myId = Swarm?.NodeID ?? ID;

        const client = await redis(DEFAULT_CONF.redis);
        await client.del('eavesdocker:' + myId);

        app.setup({
            nodeHealthThreshold: 1,
            health: { $inherit: 'redis' },
            pipeline: [ { emit: { event: 'emit' } } ]
        });
        await app.start();

        const json = await client.get('eavesdocker:' + myId);
        assert.strictEqual(myId, JSON.parse(json).id);
        await app.stop();

        await sleep(2000);

        const json2 = await client.get('eavesdocker:' + myId);
        assert(!json2);

        await client.close();
    });

    it('Should respond to ping-pong redis message', async function(){
        this.timeout(10000);

        const { ID, Swarm } = await docker.info();
        const myId = Swarm?.NodeID ?? ID;

        const client = await redis(DEFAULT_CONF.redis, true);

        app.setup({
            health: { $inherit: 'redis' }
        });
        await app.start();

        const p =  new Promise(done => client.sub.subscribe('eavesdocker:nodes:pong', function(msg){
            const node = JSON.parse(msg);
            assert.strictEqual(node.id, myId);
            done();
        }))

        client.publish('eavesdocker:nodes:ping');

        await p;

        await app.stop();
        await client.close();
    });

});


describe('Regression', function(){

    it('Should NOT spam health redis publish', async function(){
        this.timeout(10000);

        const client = await redis(DEFAULT_CONF.redis);

        app.setup({
            health: { $inherit: 'redis' }
        });
        await app.start();

        let count = 0;

        client.subscribe('eavesdocker:nodes', function(){
            count++;
        });

        await sleep(1000);

        await app.stop();

        await client.close();
        assert.strictEqual(count, 0);
    });

});

// after(function(){
//     wtf.dump();
// });