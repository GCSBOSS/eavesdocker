# Eavesdocker Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v0.0.13] - 2023-12-04

### Changed
- `level` filter to be case insensitive
- level handling to always transform it to lower case

## [v0.0.12] - 2023-08-13

### Fixed
- bug trying to obtain Cluster id on worker nodes crashing the app

## [v0.0.11] - 2023-08-12

### Added
- redis node ping/pong feature
- cluster id and manager flag to node health data when in swarm mode

### Fixed
- node and container health spamming redis publish

### Changed
- Gitlab Alert transport to use a JSON description instead of html

## [v0.0.10] - 2023-07-31

### Fixed
- node health info not refreshing correctly

### Added
- missing exception when redis channel is not set
- GitLab Alert transport
- missing error info to log entry when failing to attach to a container
- config item for the node health threshold

### Changed
- pipeline building to fail when a bad job type is found
- node health redis key to a full record instead of just the node name

## [v0.0.9] - 2023-07-25

### Fixed
- container health redis key not refreshing

### Added
- config for the container health threshold

## [v0.0.8] - 2023-07-25

### Fixed
- pipeline attempting to parse JSON twice when doing replace inside branch

## [v0.0.7] - 2023-07-25

### Fixed
- startup to only start listening to docker events after gathering node info
- bug on 'time' transform when specified field is undefined

### Added
- flow branch job
- raw string replacement job

### Changed
- pipeline processing to also affect raw entries
- 'includeSource' job definition to be symmetric to other types

### Removed
- twilio transport

## [v0.0.6] - 2022-03-22

### Fixed
- pipeline being halted at first transport

## [v0.0.5] - 2022-03-22

### Added
- redis publish regularly to sinalize node health
- redis publish regularly to sinalize each container's health
- system info and more details to node and container objects
- node name to source log field transformation

### Changed
- logic to store all containers but only attach to matched ones

### Fixed
- app crash on error during transport

## [v0.0.4] - 2021-08-25

### Added
- templated `headline` to twilio transport
- `apply` transform

### Fixed
- HTML representation to escape HTML Entities on string values

### Changed
- level filter to treat non-standard log levels as 'debug'
- HTML representations to wrap nulls and booleans in a `<code>` tag
- GitLab transport to use HTML instead of markdown

## [v0.0.3] - 2021-08-21

### Changed
- default port of docker daemon config to 2375

### Fixed
- `level` filter bug of reverse condition

## [v0.0.2] - 2021-08-20

> Moving back to 0.0.x due to rapidly changing directions

### Changed
- redis keys created by health module

### Fixed
- gitlab transport that was not sending proper JSON mime-type headers

## [v0.3.0] - 2021-08-20

### Added
- twilio transport to send SMS
- support to yml config files
- templating to GitLab `title` and `labels` settings
- templating to email `subject` setting

### Changed
- entire configuration schema from `tasks` to `pipeline` key

## [v0.2.0] - 2021-08-18

### Added
- feature to maintain online containers info up to date on redis
- `event` setting to `emit` transport to define the emitted event name

### Changed
- log entries without level now default to `debug`
- tasks configuration schema

### Removed
- inspector UI
- swarm functionality

## [v0.1.11] - 2021-08-18

### Added
- feature to filter log entries by level

### Fixed
- docker log sockets not being closed correctly
- mongo transport loosing entries because of queue swapping

## [v0.1.10] - 2021-08-10

### Added
- transport to send an HTTP request on log entry
- transport to send an e-mail on log entry
- transport to create a GitLab issue on log entry
- log entries for transport errors

### Fixed
- UI: 'message' key being treated as json when it's not

## [v0.1.9] - 2021-07-19

### Added
- GUI: missing handlers for boolean and object field types in the UI

### Fixed
- bug merging swarm containers on container list endpoint
- synching between instances in a swarm

## [v0.1.8] - 2021-07-19

### Fixed
- bug listing container across a swarm cluster

## [v0.1.7] - 2021-07-19

### Fixed
- container list endpoint ignoring swarm siblings

## [v0.1.6] - 2021-07-19

### Added
- GUI: Escape key binding to list search field

### Fixed
- wrong redis key being used for swarm mode
- container data not clean on redis store in swarm mode

## [v0.1.5] - 2021-07-19

### Added
- GUI: list search feature
- GUI: camel-case handling on log entry keys

### Fixed
- GUI: reversed service focus list settings

## [v0.1.4] - 2021-07-17

### Added
- GUI: setting to hide specific fields on all log entries in a list

### Fixed
- GUI: bad spacing on no container message
- GUI: bug trying to parse JSON when something starts with curly-braces
- GUI: listener leak when deleting lists

## [v0.1.3] - 2021-07-14

### Added
- endpoint to list all active containers
- SSE endpoint to check container events and log entries in real-time
- logic to handle swarm events via redis pubsub and cache
- monitoring UI

## [v0.1.2] - 2021-04-01

### Added
- transform to ensure log entry has a date time key

### Fixed
- logs demultiplexing bug handling buffers

## [v0.1.1] - 2021-03-30

### Fixed
- api logs stream demultiplexing mishandling frame headers
- message trailing new line being preserved for non-json log entry

## [v0.1.0] - 2021-03-29

### Added
- transform pipeline feature
- throttling to mongodb transport through buffering
- redis pub transport

### Fixed
- eavesdocker adding it's own container to prevent feedback
- crash when adding container with logging driver set to 'none'

### Changed
- container identification on stdout logs to be more informative

## [v0.0.1] - 2020-08-20
- First officially published version.

[v0.0.1]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.0.1
[v0.1.0]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.1.0
[v0.1.1]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.1.1
[v0.1.2]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.1.2
[v0.1.3]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.1.3
[v0.1.4]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.1.4
[v0.1.5]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.1.5
[v0.1.6]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.1.6
[v0.1.7]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.1.7
[v0.1.8]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.1.8
[v0.1.9]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.1.9
[v0.1.10]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.1.10
[v0.1.11]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.1.11
[v0.2.0]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.2.0
[v0.3.0]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.3.0
[v0.0.2]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.0.2
[v0.0.3]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.0.3
[v0.0.4]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.0.4
[v0.0.5]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.0.5
[v0.0.6]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.0.6
[v0.0.7]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.0.7
[v0.0.8]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.0.8
[v0.0.9]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.0.9
[v0.0.10]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.0.10
[v0.0.11]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.0.11
[v0.0.12]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.0.12
[v0.0.13]: https://gitlab.com/GCSBOSS/eavesdocker/-/tags/v0.0.13
