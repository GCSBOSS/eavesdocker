
# [Eavesdocker](https://gitlab.com/GCSBOSS/eavesdocker)

Collect logs from docker containers stdout and forward to various destinations according to your configuration.


## Features

- Filter which services or stacks you want to include in the processing
- Build simple or complex processing paths for various types of log entries
- Automatic conversion to JSON entries
- Forwards logs to many destinations
  - Mongo Collection
  - Redis Pub/Sub Channel
  - E-mail (SMTP Server)
  - New GitLab Issue
  - GitLab Alert
  - Webhook
  - we are still adding more transports...
- Monitor container and swarm nodes health using Redis

## Get Started

1. Create an entry on the [requestcatcher.com] website so we can test a webhook transport.
2. Create a simple test configuration file `config.yml`:
  ```yml
  pipeline:
    - webhook:
        url: yoururl.requestcatcher.com
        headers:
          X-Foo: bar
  ```
3. Run the following command
  ```
  docker run -v "/var/run/docker.sock:/var/run/docker.sock" -v "config.yml:/config.yml" gcsboss/eavesdocker -c /config.yml`
  ```
4. Run any container that dumps something to stdout: `docker run hello`

## Install on Swarm

1. Create a [configuration file](#configuration) with YAML.
2. Add the configuration file to your swarm.
  ```yml
  configs:
    eavesdocker.yml:
      file: ./eavesdocker.yml
  ```
3. Add Eavesdocker service globally to your swarm
  ```yml
  logger:
    image: gcsboss/eavesdocker:0.0.12
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    configs: [ eavesdocker.yml ]
    command: -c /eavesdocker.yml
    deploy:
      mode: global
      restart_policy:
        condition: any
  ```
4. Generate some logs and verify your destinations

## Transports

In this section you can see the available transports and their settings.

- **Mongo Collection**
  ```yml
  - mongo:
      url: 'mongodb://myDb' # Required. Your MongoDB connection URL.
      db: 'MyDB' # Optional. Defaults to `'Eavesdocker'`. Which database to connect to.
      collection: 'MyCollection' # Optional. Defaults to `'Log_Entries'`. Which collection to send log entries to.
  ```

- **Redis Pub/Sub**
  ```yml
  - redispub:
      host: 'my-redis-host' # Optional. Defualts to `'localhost'`. The Domain/IP of you Redis server.
      port: 1111 # Optional. Defaults to `6379`. Redis connection port.
      password: 'mypass' # Optional. Defaults to `null`. The redis password in case auth is setup on the server.
      channel: 'redis:__channel' # Required. Redis channel where to publish messages.
  ```

- **Webhook**
  ```yml
  - webhook:
      url: 'http://example.com/somePath' # Required. The URL to call.
      headers: # Optional. A set of HTTP headers in key-value format.
        x-my-header: someValue
  ```

- **New GitLab Issue**
  ```yml
  - gitlab:
      url: 'https://my-gitlab.com' # Optional. Defaults to GitLab.com. The URL of your GitLab instance.
      project: 232342 # Required. Your Project ID or path string.
      token: 'glpat-aaaaaaaa' # Optional. Defaults to `null`. A token to authenticate in GitLab.
      labels: # Optional. Defaults to `[]`. A list of labels to be added to the new issue. Strings are templated.
        - label-a
        - something-{key}-cool
      title: 'some {key} title' # Optional. Defaults to `'{level} - {message}'`. A template string to generate the new issue title.

  ```

- **GitLab Alert**
  ```yml
  - gitlabAlert:
      url: 'https://my-gitlab.com/endpoint/etc...' # Required. The Alert integration endpoint from your GitLab project.
      key: 'something' # Required. The GitLab Alert integration Authorization Key.
      title: 'some {key} title' # Optional. Defaults to `'env - {message}'`. A template string to generate the alert title.
      service: 'some {key} service' # Optional. Defaults to `null`. A template string to generate the alert service.
      env: 'my-env' # Optional. A GitLab environment name.
      fingerprintFields: # Optional. Defaults to `[]`. An array of log entry fields to be considered for fingerprinting.
        - keyA
        - keyB
  ```

- **SMTP**
  ```yml
  - email:
      subject: 'some {key} subject' # Optional. Defaults to `'{level} - {message}'`. A template string to generate the subject line.
      to: 'me@example.com' # Required. A mail recipient.
      from: 'eavesdocker@my-address.com' # Required. A mail sender address.
      host: 'smtp.example.com' # Required. Your SMTP host.
      port: 465 # Required. Your SMTP port.
      secure: true # Optional. Defaults to `false`. Whether to use TLS.
  ```

## Contribute

In order to contribute you can:

- [Open issues](https://gitlab.com/GCSBOSS/eavesdocker/-/issues/new) to request features or report bugs.
- Help building the project by:
  1. [forking the repo](https://gitlab.com/GCSBOSS/eavesdocker/-/forks/new)
  2. coding in a branch
  3. [filing a merge request](https://gitlab.com/GCSBOSS/eavesdocker/-/merge_requests/new)
  4. working with me through the review process