const { routeMessage } = require('./transport');
const { shouldAttachContainer } = require('./pipeline');

const os = require('os');

async function addContainer(c){

    if(c.id.substr(0, 12) == os.hostname())
        return;

    c.stack = c.labels['com.docker.stack.namespace'] || c.labels['com.docker.compose.project'] || '';
    c.service = c.labels['com.docker.swarm.service.name'] || c.labels['com.docker.compose.service']  || '';
    c.service = c.service.replace(c.stack + '_', '');
    c.node = this.global.node.name;

    c.number = (c.labels['com.docker.swarm.task.name'] ||
        c.labels['com.docker.compose.container-number'] || '1').replace(/.*?\./, '')
        .replace(/\..*$/, '');

    c.cid = '\'' + c.stack + '\' > ' + c.service + ' (' + c.id.substr(0, 8) + ')';

    try{
        if(shouldAttachContainer.call(this, c)){
            c.stream = await this.global.docker.logs(c.id, routeMessage.bind(this, c));
            this.log.debug('Attached to %s', c.cid);
        }
        this.global.containers[c.id] = c;

        if(this.global.healthRedis){
            const json = JSON.stringify({ ...c, stream: undefined });
            this.global.healthRedis.publish('eavesdocker:containers', json);

            await this.global.healthRedis.set('eavesdocker:containers:' + c.id.substr(0, 12), json, {
                EX: this.conf.containerHealthThreshold
            });
        }
    }
    catch(err){
        this.log.warn({ err }, 'Failed to attach to %s', c.cid);
    }
}

async function removeContainer(id){
    await new Promise(done => setTimeout(done, 1000));
    if(!this.global.containers[id])
        return;

    this.global.containers[id].stream?.destroy();
    this.log.debug('Dettached from %s', this.global.containers[id].cid);
    delete this.global.containers[id];

    if(global.healthRedis)
        global.healthRedis.del('eavesdocker:containers:' + id.substr(0, 12));
}

async function parseEvent(data){
    const event = JSON.parse(data);
    const actionType = event.Action + ' ' + event.Type;
    if(actionType == 'start container'){
        const c = await this.global.docker.call('/containers/' + event.id + '/json');
        addContainer.call(this, {
            id: c.Id,
            image: c.Config.Image,
            createdAt: new Date(c.Created),
            labels: c.Config.Labels
        });
    }
    else if(actionType == 'die container')
        removeContainer.call(this, event.id);
}

function destroyLoggers(){
    for(const id in this.global.containers)
        this.global.containers[id].stream?.destroy();
}

module.exports = { parseEvent, addContainer, destroyLoggers };
