const { TRANSPORTS, createTransport } = require('./transport');

const LEVELS =  { trace: 0, debug: 1, info: 2, warn: 3, error: 4, fatal: 5 };

async function createStage({ call }, jobs, root = false){

    let fn = '';
    let parsed = false;

    if(root){
        fn += 'let alreadyParsed = false;\n';
        fn += 'const LEVELS = ' + JSON.stringify(LEVELS) + ';\n';
    }

    for(const j of jobs){

        const type = Object.keys(j)[0];

        if(parsed && [ 'replace' ].includes(type))
            throw new Error(`Job of type '${type}' cannot be called after the input is already transformed`);

        if(!parsed && ([ 'time', 'level', 'envelope', 'apply', 'includeSource' ].includes(type) || type in TRANSPORTS)){
            parsed = true;
            fn += 'if(!alreadyParsed){\ntry{\ne = JSON.parse(e);\nalreadyParsed = true;\n}\ncatch(_err){}\n}\n' +
                'if(typeof e !== \'object\')\n    e = { message: e };\n' +
                'e.level = typeof e.level == \'string\' ? e.level.toLowerCase() : \'debug\';\n';
        }

        if(type == 'stack')
            fn += `if(c.stack != '${j.stack}') return;\n`;

        else if(type == 'services')
            fn += `if(!'|${j.services.join('|')}|'.includes('|' + c.service + '|')) return;\n`;

        else if(type == 'level'){
            let level = j.level;
            if(typeof level == 'string')
                level = { from: level };

            fn += 'if(false';
            if(level.from)
                fn += ` || (LEVELS[e.level] || 0) < ${LEVELS[level.from.toLowerCase()]}`;
            if(level.to)
                fn += ` || (LEVELS[e.level] || 0) > ${LEVELS[level.to.toLowerCase()]}`;
            fn += ') return;\n';
        }
        else if(type == 'replace')
            fn += `e = e.replaceAll(${JSON.stringify(String(j.replace))}, ${JSON.stringify(String(j.with))});\n`;
        else if(type == 'time'){
            fn += `const time = new Date(e?.['${j.time}'] ?? NaN);\n`;
            fn += `e['${j.time}'] = isNaN(time) ? new Date() : time;\n`;
        }
        else if(type == 'envelope')
            fn += `e = { '${j.envelope}': e };\n`;
        else if(type == 'apply')
            fn += `Object.assign(e, ${JSON.stringify(j.apply)});\n`;

        else if(type == 'includeSource')
            fn += 'e.source = { node: this.global.node.name, stack: c.stack, service: c.service, id: c.id };\n';

        else if(type in TRANSPORTS){
            const name = await call(createTransport, j[type], type);
            fn += `try {
                Promise.resolve(this.global.transports.${name}.push(e)).catch(err =>
                    this.log.error({ err }));
            } catch(err){
                this.log.error({ err });
            }\n`;
        }

        else if(type == 'branch')
            fn += '(() => {\n' + await call(createStage, j.branch) + '\n})();\n';

        else
            throw new Error(`Unkown job type '${type}'`);
    }

    return fn;
}

function shouldAttachContainer(c){
    const m = this.global.matcher;
    return (!m.stack || m.stack == c.stack) &&
        (!Array.isArray(m.services) || m.services.includes(c.service));
}

async function buildPipelineFunction({ call, conf }){
    const fn = await call(createStage, conf.pipeline, true);
    const AsyncFunction = Object.getPrototypeOf(async () => {}).constructor;
    return new AsyncFunction('e', 'c', fn).bind(this);
}

module.exports = {
    shouldAttachContainer,
    buildPipelineFunction
}
