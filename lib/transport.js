
function buildTemplater(template){
    const parts = template.split(/{[a-zA-Z0-9_\-\.]+}/g);
    const vars = (template.match(/(?<={)[a-zA-Z0-9_\-\.]+(?=})/g) || []).map(v =>
        v.split('.').map(ident => `['${ident}']`).join());

    let fn = 'return ';
    for(let i = 0; i < vars.length; i++)
        fn += `'${parts.shift()}' + d${vars.shift()} + `;

    fn += `'${parts.shift()}'`;

    return new Function('d', fn);
}

function htmlObject(obj){
    let body = '<ul>';

    for(const key in obj){
        body += '<li><b>' + key + ':</b> ';

        if(obj[key] instanceof Date)
            body += obj[key].toISOString();
        else if(obj[key] === null || typeof obj[key] == 'boolean')
            body += '<code>' + String(obj[key]) + '</code>';
        else if(typeof obj[key] == 'object')
            body += htmlObject(obj[key]);
        else
            body += String(obj[key]).replace(/[\u00A0-\u9999<>\&]/g,
                i => '&#' + i.charCodeAt(0) + ';');

        body += '</li>'
    }

    return body + '</ul>';
}

const TRANSPORTS = {

    async mongo(conf){
        const { MongoClient } = require('mongodb');
        const client = new MongoClient(conf.url, {
            useUnifiedTopology: true,
            serverSelectionTimeoutMS: 3000
        });
        await client.connect();
        const db = client.db(conf.db || 'Eavesdocker');
        const coll = db.collection(conf.collection || 'Log_Entries');
        let queue = [];
        const it = setInterval(function(){
            const readyDocs = queue;
            queue = [];
            readyDocs.length > 0 && coll.insertMany(readyDocs);
        }, 1500);
        return {
            close: async () => {
                clearInterval(it);
                queue.length > 0 && await coll.insertMany(queue);
                await client.close();
            },
            push: data => queue.push(data)
        }
    },

    async redispub(conf){
        if(!conf.channel)
            throw new Error('Missing redis channel');

        const redis = require('nodecaf-redis');
        const client = await redis(conf);
        return {
            close: () => client.close(),
            push: data => client.publish(conf.channel, JSON.stringify(data))
        }
    },

    webhook(conf){
        const { Pool } = require('muhb');
        const pool = new Pool({ size: 100, timeout: 4000 });
        const headers = { ...conf.headers || {}, 'Content-Type': 'application/json' };

        if(typeof headers != 'object')
            throw new Error('Headers config must be of type \'object\'');

        return {
            close: Function.prototype,
            push: data => pool.post(conf.url, headers, data)
        }
    },

    emit(conf){
        return {
            close: Function.prototype,
            push: msg => process.emit(conf.event || 'eavesdocker', msg)
        }
    },

    gitlab(conf){
        const { Pool } = require('muhb');
        const pool = new Pool({ size: 100, timeout: 4000 });

        const url = conf.url || 'https://gitlab.com';
        const path = url + '/api/v4/projects/' + encodeURIComponent(conf.project) + '/issues';
        const headers = { 'Content-Type': 'application/json',
            'PRIVATE-TOKEN': conf.token };

        const labelers = conf.labels.map(buildTemplater);
        const titler = buildTemplater(conf.title || '{level} - {message}');

        return {
            close: Function.prototype,
            push: async data => {

                const { status } = await pool.post(path, headers, {
                    title: titler(data),
                    labels: labelers.map(l => l(data)),
                    'created_at': new Date(),
                    description: htmlObject(data)
                });

                if(status > 299)
                    throw new Error('GitLab Responded with status ' + status);
            }
        }
    },

    gitlabAlert(conf){
        const { Pool } = require('muhb');
        const pool = new Pool({ size: 100, timeout: 4000 });

        if(!conf.key)
            throw new Error('Missing GitLab alerts Authorization Key');

        const headers = {
            Authorization: 'Basic ' + btoa(':' + conf.key),
            'Content-Type': 'application/json'
        };

        const titler = buildTemplater(conf.title || conf.env + ' - {message}');
        const servicer = conf.service
            ? buildTemplater(conf.service)
            : () => undefined;

        const fpFields = conf.fingerprintFields ?? [];

        const crypto = require('crypto');

        return {
            close: Function.prototype,
            push: async data => {
                const hash = crypto.createHash('sha1');
                hash.update(fpFields.map(f => data[f]).join(','));

                const { status } = await pool.post(conf.url, headers, {
                    title: titler(data),
                    description: JSON.stringify(data),
                    // hosts:
                    service: servicer(data),
                    'monitoring_tool': 'eavesdocker',
                    severity: {
                        fatal: 'critical',
                        error: 'high',
                        warn: 'low',
                        info: 'info',
                        debug: 'unknown'
                    }[data.level.toLowerCase()],
                    fingerprint: hash.digest('hex'),
                    'gitlab_environment_name': conf.env
                });

                if(status > 299)
                    throw new Error('GitLab Responded with status ' + status);
            }
        }
    },

    email(conf){
        const nodemailer = require('nodemailer');
        const transporter = nodemailer.createTransport(conf);

        if(!conf.to)
            throw new Error('Recipient not defined');

        const subjecter = buildTemplater(conf.subject || '{level} - {message}');

        return {
            close: () => transporter.close(),
            push: data => transporter.sendMail({
                from: conf.from || 'Eavesdocker <eavesdocker@example.com>',
                to: conf.to,
                subject: subjecter(data),
                text: JSON.stringify(data, null, 4),
                html: htmlObject(data),
            })
        };
    }

};

async function createTransport({ log, transports, conf }, spec, type){
    const name = type + Object.keys(transports).length;

    if(typeof spec.$inherit == 'string'){
        spec = { ...conf[spec.$inherit], ...spec };
        delete spec.$inherit;
    }

    transports[name] = await TRANSPORTS[type].call(this, spec);
    log.debug('Installed transport \'%s\' (%s)', name, type);
    return name;
}

module.exports = {

    TRANSPORTS,

    createTransport,

    routeMessage(container, entry){
        this.global.lines++;
        this.global.processEntry(entry, container);
    },

    async closeTransports(){
        for(const name in this.global.transports)
            await this.global.transports[name].close();
    }

}
