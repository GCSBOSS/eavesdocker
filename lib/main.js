const Nodecaf = require('nodecaf');
const redis = require('nodecaf-redis');
const os = require('os');

const { addContainer, parseEvent, destroyLoggers } = require('./container');
const { closeTransports, TRANSPORTS } = require('./transport');
const { buildPipelineFunction } = require('./pipeline');
const dockerAPI = require('./docker');

const MATCHERS = { stack: 1, services: 1 };

async function fetchNodeInfo({ docker, containers }){
    const info = await docker.call('/info');
    const data = {
        os: info.OperatingSystem,
        cpus: info.NCPU,
        memory: info.MemTotal,
        name: info.Name,
        id: info.Swarm?.NodeID ?? info.ID,
        containers: Object.keys(containers).length,
        freeMemory: os.freemem()
    };

    // https://github.com/moby/moby/issues/32166
    // if(info.Swarm?.NodeID){
    //     data.cluster = info.Swarm.Cluster?.ID;
    //     if(info.Swarm.RemoteManagers.find(n => n.NodeID == info.Swarm.NodeID))
    //         data.manager = true;
    // }

    return data;
}

function setNodeHealth({ conf, healthRedis, node }){
    const json = JSON.stringify(node);
    healthRedis.set('eavesdocker:' + node.id, json, {
        EX: conf.nodeHealthThreshold
    });
    healthRedis.publish('eavesdocker:nodes', json);
}

module.exports = () => new Nodecaf({

    conf: {
        pipeline: [],
        docker: {
            port: 2375,
            timeout: 2000
        }
    },

    async startup({ global, conf, call }){
        global.containers = {};
        global.transports = {};

        global.matcher = {};
        for(const job of conf.pipeline){
            const type = Object.keys(job)[0];
            if(type in MATCHERS)
                global.matcher[type] = job[type];
            else if(type in TRANSPORTS)
                break;
        }

        global.docker = dockerAPI(conf.docker);
        global.lines = 0;

        global.processEntry = await call(buildPipelineFunction);
        global.node = await call(fetchNodeInfo);

        global.events = await global.docker.events(parseEvent.bind(this));

        if(typeof conf.health == 'object'){

            conf.containerHealthThreshold = conf.containerHealthThreshold ?? 30
            conf.nodeHealthThreshold = conf.nodeHealthThreshold ?? 140

            const nodeHealthInterval = conf.nodeHealthThreshold - Math.min(5, conf.nodeHealthThreshold / 3);
            const containerHealthInterval = conf.containerHealthThreshold - Math.min(5, conf.containerHealthThreshold / 3);

            if(typeof conf.health.$inherit == 'string'){
                conf.health = { ...conf[conf.health.$inherit], ...conf.health };
                delete conf.health.$inherit;
            }

            global.healthRedis = await redis(conf.health, true);

            call(setNodeHealth);

            global.healthNodeInt = setInterval(async () => {
                global.node = await call(fetchNodeInfo);
                call(setNodeHealth);
            }, nodeHealthInterval * 1000);

            global.healthContInt = setInterval(() => {
                for(const id in global.containers){
                    const json = JSON.stringify({ ...global.containers[id], stream: undefined });
                    global.healthRedis.publish('eavesdocker:containers', json);
                    global.healthRedis.expire('eavesdocker:containers:' + id.substring(0, 12),
                        conf.containerHealthThreshold);
                }
            }, containerHealthInterval * 1000);

            global.healthRedis.sub.subscribe('eavesdocker:nodes:ping', function(){
                global.healthRedis.publish('eavesdocker:nodes:pong', JSON.stringify(global.node));
            });
        }

        const containers = await global.docker.call('/containers/json');
        for(const c of containers)
            await addContainer.call(this, {
                id: c.Id,
                image: c.Image,
                createdAt: new Date(c.Created),
                labels: c.Labels,
            });
    },

    async shutdown({ global }){
        if(global.healthRedis){
            global.healthRedis.sub.unsubscribe('eavesdocker:nodes:ping');
            clearInterval(global.healthNodeInt);
            clearInterval(global.healthContInt);
            await global.healthRedis.close();
        }

        global.events.removeAllListeners();
        global.events.destroy();
        destroyLoggers.call(this);
        await closeTransports.call(this);
    }

});
